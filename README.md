# Repository for trying various GP methods with dvc and mlflow

### GPs
- TODO: write evaluation function that takes (posterior) distribution and plots samples & confidence intervals
- TODO: function that determines maximum based on mean function of posterior GP

### MLOps
- TODO: after having $\geq 2$ datasets and $\geq 2$ methods, start using dvc with parallel dvc repro stages
- TODO: include mlflow for some metrics (eg distance predicted maximum -- actual)